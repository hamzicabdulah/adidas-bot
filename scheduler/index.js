var asyncInterval = require('asyncinterval')
var jobs = {}

var addJob = function (name, job, args, interval, timeout, timeoutFn) {
  jobs[name] = asyncInterval(function (done) {
    job(done, args)
  }, interval || 10000, timeout || 100000)
  
  jobs[name].onTimeout(timeoutFn || function () { console.log('Job ' +name+ ' timed out') })
}

var killJob = function (name) {
  jobs[name].clear()
}

var killJobSoon = function (name, time) {
  setTimeout(function () { killJob(name) }, time)
}


var listJobs = function () {
    
    return Object.keys(jobs)
}

module.exports = function () {
  return {
    addJob: addJob,
    killJob: killJob,
    killJobSoon: killJobSoon,
    listJobs: listJobs
  }
}
