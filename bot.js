require('dotenv').config()
var express = require('express')
var bodyParser = require('body-parser')
var app = express()
var slack = require('./slack')()
var scheduler = require('./scheduler')()

app.use(bodyParser())// get html form data
app.use(bodyParser.json()) // get JSON data
app.use(express.static('public'))

app.get('/', function (req, res) {
  res.sendfile('./index.html', {root: __dirname})
})

require('./scan')(scheduler, slack)
require('./proxy')(slack)
require('./account')(slack)

app.listen(8082, '0.0.0.0', function () {
  console.log('Listening on 808')
})
