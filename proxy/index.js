var request = require('request')

module.exports = function(slack) {
    
    slack.controller.hears('proxy:add', ['mention','direct_mention', 'direct_message'], function(bot, msg) {
        var bodyObject = {}, botResponseMessage;

        bot.startConversation(msg, function(err, convo) {
            if(!err) {
                //Taking from the user all the information that's supposed to be sent in the req.body 
                convo.say('Okay. Give us your information.')
                convo.ask('Insert IP address:', function(response, convo) {
                    bodyObject.addr = response
                    convo.next()
                })
                convo.ask('Insert port:', function(response, convo) {
                    bodyObject.port = response
                    convo.next()
                })
                convo.ask('Insert name:', function(response, convo) {
                    bodyObject.name = response
                    convo.next()
                })
                convo.ask('Insert username:', function(response, convo) {
                    bodyObject.user = response
                    convo.next()
                })
                convo.ask('Insert password:', function(response, convo) {
                    bodyObject.pass = response
                    convo.next()
                })
                /* Real API request commented out due to API not currently working
                var options = {
                    url: 'http://adidas-api-public.knyz.org/api/add/proxy',
                    headers: {
                        'User-Agent': 'request',
                        'Content-Type': 'application/json'
                    },
                    body: bodyObject
                }

                request.post(options, function(err, res, body) {
                    if(err) console.log('err')
                    //The body should be an object with one property, named "status", with value 0 or 1
                    botResponseMessage = botResponse(body)
                })
                */

                botResponseMessage = botAddResponse({'status': 1})
                convo.say(botResponseMessage)
                convo.next()
            }
        })
    })

    slack.controller.hears('proxy:delete (.*)', ['mention','direct_mention', 'direct_message'], function(bot, msg) {
        var name = msg.match[1], botResponseMessage;

        /* Real API request commented out due to API not currently working
        var options = {
            url: 'http://adidas-api-public.knyz.org/api/delete/proxy/' + name,
            headers: {
                'User-Agent': 'request',
                'Content-Type': 'application/json'
            }
        }

        request(options, function(err, res, body) {
            if(err) console.log('err')
            //The body should be an object with one property, named "status", with value 0 or 1
            botResponseMessage = botDeleteResponse(body)
        })
        */

        botResponseMessage = botDeleteResponse({'status': 1})
        bot.reply(msg, botResponseMessage)
    })

}

function botAddResponse(body) {   
    //Return success message if body's status property is 1, or fail message if status property is 0
    var message = body.status === 1 ? 'Registration successful.' : 'Registration failed. Try again.'
    return message
}

function botDeleteResponse(body) {   
    //Return success message if body's status property is 1, or fail message if status property is 0
    var message = body.status === 1 ? 'Deletion successful.' : 'Deletion. Try again.'
    return message
}