
var request = require('request')
var options = {
  url: '"http://adidas-api-public.knyz.org/api/search/"',
  headers: {
    'User-Agent': 'request',
    'Content-Type': 'application/json'
  }
}

module.exports = function (botStream, slack) {
    slack.controller.hears('search', ['mention','direct_mention', 'direct_message'], function(bot, msg) {
       request({
                url: 'http://adidas-api-public.knyz.org/api/search/'+ msg.text.split(' ').slice(1).join('+'),
                headers: {
                    'User-Agent': 'request',
                    'Content-Type': 'application/json'
                }
            }, function(err, res, body){
                        console.log(fomatAttachment(body))
                        bot.reply(msg, fomatAttachment(body))
                    })
                    })
                }


var fomatAttachment = function (body) { 
    body = JSON.parse(body)
    var result = {}
    result.app = 'Adidas AE'
    result.text = 'Search...'
    result.pretext = body.terms.join(',')
    result.url = body.url
    result.fullText = 'Search for keywords on adidas'
    result.fields = [],
    body.items.forEach(function(val){
        result.fields.push({title: val.name, value: val.link, short:true})
    })
    result.img = '' 
     
    return {
        'username': 'Sneaker Best ATC', 
        'mrkdwn': true,
        'attachments': [
                {
                    'fallback': '@' + result.app + ' on Sneaker Best says: ' + result.text,
                    'color': '#36a64f',
                    'pretext': result.pretext,
                    'author_name':  result.app,
                    'author_link': result.app,
                    'author_icon': 'http://flickr.com/icons/bobby.jpg',
                    'title': result.url,
                    'title_link':'',
                    'text': result.text,
                    'fields':result.fields,
                    'image_url': result.img,
                    'thumb_url': 'http://example.com/path/to/thumb.png',
                    'footer': result.app,
                    'mrkdwn_in' : ['text'],
                    'footer_icon': 'https://platform.slack-edge.com/img/default_application_icon.png'
                }
        ]
    }
}

