var Botkit = require('botkit');

module.exports = function(){
var controller = Botkit.slackbot({
    debug: false,
    clientId: process.env.clientId,
    clientSecret: process.env.clientSecret,
    scopes: ['bot','incoming-webhook'],
    send_via_rtm: true,
    require_delivery: true,
});

var bot = controller.spawn({
    token: process.env.token
}).startRTM();


return {
    bot: bot,
    controller: controller
}
}